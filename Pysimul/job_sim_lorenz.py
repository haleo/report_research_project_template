"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_lorenz.py
```
"""

import matplotlib.pyplot as plt
from pathlib import Path

from fluidsim.solvers.models0d.lorenz.solver import Simul

here = Path(__file__).absolute().parent
params = Simul.create_default_params()

params.rho = 28

params.time_stepping.deltat0 = 0.02
params.time_stepping.t_end = 20


params.output.periods_print.print_stdout = 0.01

sim = Simul(params)
sim1 = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs0 + 2.0)
sim.state.state_phys.set_var("Y", sim.Ys0 - 1.0)
sim.state.state_phys.set_var("Z", sim.Zs0 - 10)


# sim.output.phys_fields.plot()
sim.time_stepping.start()


sim.output.print_stdout.plot_XY()
#sim.output.print_stdout.plot_XY_vs_time()
# see also the other plot_* methods of sim.output.print_stdout

# =============================================================================
ax = plt.gca()
fig = ax.figure

fig_dir = here.parent / "fig"
fig_dir.mkdir(exist_ok=True)

fig.savefig(fig_dir / "fig.png")
# =============================================================================

plt.show()
