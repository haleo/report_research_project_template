from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt

from fluidsim.solvers.models0d.predaprey.solver import Simul

here = Path(__file__).absolute().parent

params = Simul.create_default_params()


xs = np.linspace(0.01, 5, 100)
ys = np.linspace(0.01, 5, 100)

Xs, Ys = np.meshgrid(xs, ys)

potential = (
    params.C * np.log(Xs) - params.D * Xs + params.A * np.log(Ys) - params.B * Ys
)

fig, ax = plt.subplots()

ax.contour(Xs, Ys, potential, 10)

ax.set_xlabel("$X$")
ax.set_ylabel("$y$")

ax.set_title("Isolines of potential for the prey-predator model")

tmp_fig = here.parent / "fig"
tmp_fig.mkdir(exist_ok=True)

fig.savefig(tmp_fig / "fig_isocontour.png")

plt.show()
